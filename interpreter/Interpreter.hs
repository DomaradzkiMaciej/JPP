module Interpreter where

import Data.Map as Map
import Data.Maybe
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Except
import System.IO (hPutStrLn, stderr)

import AbsDuc
import TypesAndData
import EvalExp
import EvalDecl
import EvalStmt

runProgram :: Program-> IO ()
runProgram (Prog stmts) = do
  let initialState = Map.singleton 0 (INTEGER 1)
  let initialEnv = (Map.empty, Map.empty)
  let statement = runReaderT (evalStmts stmts) initialEnv

  finalValue <- runExceptT $ execStateT statement initialState
  case finalValue of
    Left a -> hPutStrLn stderr a
    Right b -> return ()
