module EvalExp where

import Data.Map as Map
import Data.Maybe
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Except

import AbsDuc
import PrintDuc ( printTree )
import TypesAndData


evalERel :: (Int -> Int -> Bool) -> String -> Expr -> Expr -> Semantic (Maybe NVoidVal)

evalERel op opStr exp1 exp2 = do
  val1 <- evalExp exp1
  case val1 of
    Just (INTEGER val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (INTEGER val2) -> return . Just . BOOL $ op val1 val2
        _ -> throwError $ "Error: Usage of operator " ++ opStr ++ " with non int value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of operator " ++ opStr ++ " with non int value\n" ++ printTree exp1

evalExp :: Expr -> Semantic (Maybe NVoidVal)

evalExp (EVar ident) = do
  (vars, _) <- ask
  case Map.lookup ident vars of
    Just id -> gets $ Map.lookup id
    Nothing -> throwError $ "Error: Usage of undeclared variable\n" ++ show ident

evalExp (ELitInt val) = return . Just . INTEGER $ fromIntegral val

evalExp ELitTrue = return . Just $ BOOL  True

evalExp ELitFalse = return . Just $ BOOL False

evalExp (EApp ident exps) = do
  (_, funs) <- ask
  case Map.lookup ident funs of
    Just (Function fun) -> fun exps
    Nothing -> throwError $ "Error: Usage of undeclared function\n" ++ show ident

evalExp (EString val) = return . Just $ STRING val

evalExp (Neg exp) = do
  val <- evalExp exp
  case val of
    Just (INTEGER val) -> return . Just . INTEGER $ negate val
    _ -> throwError $ "Error: Usage of negation with non int value\n" ++ printTree exp

evalExp (Not exp) = do
  val <- evalExp exp
  case val of
    Just (BOOL val) -> return . Just . BOOL $ not val
    _ -> throwError $ "Error: Usage of negation with non bool value\n" ++ printTree exp

evalExp (EMul exp1 Times exp2) = do
  val1 <- evalExp exp1
  case val1 of
    Just (INTEGER val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (INTEGER val2) -> return . Just . INTEGER $ val1 * val2
        _ -> throwError $ "Error: Usage of multiplication with non int value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of multiplication with non int value\n" ++ printTree exp1

evalExp (EMul exp1 Div exp2) = do
  val1 <- evalExp exp1
  case val1 of
    Just (INTEGER val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (INTEGER 0) -> throwError $ "Error: Division by 0\n" ++ printTree exp2
        Just (INTEGER val2) -> return . Just . INTEGER $ val1 `div` val2
        _ -> throwError $ "Error: Usage of division with non int value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of division with non int value\n" ++ printTree exp1

evalExp (EMul exp1 Mod exp2) = do

  val1 <- evalExp exp1
  case val1 of
    Just (INTEGER val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (INTEGER 0) -> throwError $ "Error: modulo 0\n" ++ printTree exp2
        Just (INTEGER val2) -> return . Just . INTEGER $ val1 `mod` val2
        _ -> throwError $ "Error: Usage of modulo with non int value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of modulo with non int value\n" ++ printTree exp1

evalExp (EAdd exp1 Plus exp2) = do
  val1 <- evalExp exp1
  case val1 of
    Just (INTEGER val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (INTEGER val2) -> return . Just . INTEGER $ val1 + val2
        _ -> throwError $ "Error: Usage of addition with non int value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of addition with non int value\n" ++ printTree exp1

evalExp (EAdd exp1 Minus exp2) = do
  val1 <- evalExp exp1
  case val1 of
    Just (INTEGER val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (INTEGER val2) -> return . Just . INTEGER $ val1 - val2
        _ -> throwError $ "Error: Usage of subtraction with non int value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of subtraction with non int value\n" ++ printTree exp1

evalExp (ERel exp1 LTH exp2) = evalERel (<) "<" exp1 exp2

evalExp (ERel exp1 LE exp2) = evalERel (<=) "<=" exp1 exp2

evalExp (ERel exp1 GTH exp2) = evalERel (>) ">" exp1 exp2

evalExp (ERel exp1 GE exp2) = evalERel (>=) ">=" exp1 exp2

evalExp (ERel exp1 EQU exp2) = evalERel (==) "==" exp1 exp2

evalExp (ERel exp1 NE exp2) = evalERel (/=) "!=" exp1 exp2

evalExp (EAnd exp1 exp2) = do
  val1 <- evalExp exp1
  case val1 of
    Just (BOOL val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (BOOL val2) -> return . Just . BOOL $ val1 && val2
        _ -> throwError $ "Error: Usage of and with non bool value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of and with non bool value\n" ++ printTree exp1

evalExp (EOr exp1 exp2) = do
  val1 <- evalExp exp1
  case val1 of
    Just (BOOL val1) -> do
      val2 <- evalExp exp2
      case val2 of
        Just (BOOL val2) -> return . Just . BOOL $ val1 || val2
        _ -> throwError $ "Error: Usage of or with non bool value\n" ++ printTree exp2
    _ -> throwError $ "Error: Usage of or with non bool value\n" ++ printTree exp1