module Graph where
import Set(Set)
import qualified Set as Set
import qualified Data.List as L

class Graph g where
  empty   :: g a
  vertex  :: a -> g a
  union   :: g a -> g a -> g a
  connect :: g a -> g a -> g a

data Relation a = Relation { domain :: Set a, relation :: Set (a, a) }
    deriving (Eq, Show)

data Basic a = Empty
             | Vertex a
             | Union (Basic a) (Basic a)
             | Connect (Basic a) (Basic a)

instance Graph Relation where
  empty = Relation { domain = Set.empty, relation = Set.empty }
  vertex x = Relation { domain = Set.Singleton x, relation = Set.empty }
  union a1 a2 = Relation { domain = domain a1 <> domain a2, relation = relation a1 <> relation a2 }
  connect a1 a2 = Relation { domain = domain a1 <> domain a2,
                             relation = relation a1 <> relation a2 <> Set.cartProd (domain a1) (domain a2) }

instance (Ord a, Num a) => Num (Relation a) where
  fromInteger = vertex . fromInteger
  (+) a1 a2   = Relation { domain = Set.union' (domain a1) (domain a2),
                           relation = Set.union' (relation a1) (relation a2) }
  (*) a1 a2   = Relation { domain = Set.union' (domain a1) (domain a2),
                           relation = Set.union' (Set.union' (relation a1) (relation a2)) (Set.cartProd (domain a1) (domain a2)) }
  signum      = const empty
  abs         = id
  negate      = id

instance Graph Basic where
  empty = Empty
  vertex = Vertex
  union = Union
  connect = Connect

instance Ord a => Eq (Basic a) where
  (==) a1 a2 = if (Set.remDup . verticesToSet) a1 == (Set.remDup . verticesToSet) a2
                  && (Set.remDup . edgesToSet) a1 == (Set.remDup . edgesToSet) a2
                 then True
                 else False

verticesToSet :: (Ord a) => Basic a -> Set a
verticesToSet Empty = Set.empty
verticesToSet (Vertex a) = Set.singleton a
verticesToSet (Union a1 a2) = verticesToSet a1 <> verticesToSet a2
verticesToSet (Connect a1 a2) = verticesToSet a1 <> verticesToSet a2

edgesToSet :: (Ord a) => Basic a -> Set (a,a)
edgesToSet Empty = Set.empty
edgesToSet (Vertex a) = Set.empty
edgesToSet (Union a1 a2) = edgesToSet a1 <> edgesToSet a2
edgesToSet (Connect a1 a2) = edgesToSet a1 <> edgesToSet a2 <> Set.cartProd (verticesToSet a1) (verticesToSet a2)

instance (Ord a, Num a) => Num (Basic a) where
  fromInteger = vertex . fromInteger
  (+)         = union
  (*)         = connect
  signum      = const empty
  abs         = id
  negate      = id

instance Semigroup (Basic a) where
  (<>) = union

instance Monoid (Basic a) where
  mempty = Empty

fromBasic :: Graph g => Basic a -> g a
fromBasic Empty = empty
fromBasic (Vertex a) = vertex a
fromBasic (Union a1 a2) = union (fromBasic a1) (fromBasic a2)
fromBasic (Connect a1 a2) = connect (fromBasic a1) (fromBasic a2)

instance (Ord a, Show a) => Show (Basic a) where
  show a = let edges = (Set.remDup . edgesToSet) a
               vertices = Set.sub ((Set.remDup . verticesToSet) a) (Set.unpackTuples edges)
           in "edges " ++ Set.show' edges ++ " + vertices " ++ Set.show' vertices

example34 :: Basic Int
example34 = 1*2 + 2*(3+4) + (3+4)*5 + 17

todot :: (Ord a, Show a) => Basic a -> String
todot a = let edges = (Set.remDup . edgesToSet) a
              vertices = Set.sub ((Set.remDup . verticesToSet) a) (Set.unpackTuples edges)
              showEdges [] = ""
              showEdges ((x,y):xs) = show x ++ " -> " ++ show y ++ ";\n" ++ showEdges xs
              showVertices [] = ""
              showVertices (x:xs) = show x ++ ";\n" ++ showVertices xs
          in "digraph {\n" ++ (showEdges $ Set.toAscList edges) ++ (showVertices $ Set.toAscList vertices) ++ "}"

instance Functor Basic where
  fmap f Empty = Empty
  fmap f (Vertex a) = (Vertex . f) a
  fmap f (Union a1 a2) = Union (fmap f a1) (fmap f a2)
  fmap f (Connect a1 a2) = Connect (fmap f a1) (fmap f a2)

mergeV :: Eq a => a -> a -> a -> Basic a -> Basic a
mergeV v1 v2 v3 Empty = Empty
mergeV v1 v2 v3 (Vertex a) = if v2 == a || v1 == a then (Vertex v3) else (Vertex a)
mergeV v1 v2 v3 (Union a1 a2) = Union (mergeV v1 v2 v3 a1) (mergeV v1 v2 v3 a2)
mergeV v1 v2 v3 (Connect a1 a2) = Connect (mergeV v1 v2 v3 a1) (mergeV v1 v2 v3 a2)

instance Applicative Basic where
  pure = Vertex
  (<*>) (Vertex f) (Vertex a) = (Vertex . f) a

instance Monad Basic where
  (>>=) Empty f = Empty
  (>>=) (Vertex a) f = f a
  (>>=) (Union a1 a2) f = Union (a1 >>= f) (a2 >>= f)
  (>>=) (Connect a1 a2) f = Connect (a1 >>= f) (a2 >>= f)

splitV :: Eq a => a -> a -> a -> Basic a -> Basic a
splitV v1 v2 v3 Empty = Empty
splitV v1 v2 v3 (Vertex a) = if a == v1 then Union (Vertex v2) (Vertex v3)  else Vertex a
splitV v1 v2 v3 (Union a1 a2) = Union (splitV v1 v2 v3 a1) (splitV v1 v2 v3 a2)
splitV v1 v2 v3 (Connect a1 a2) = Connect (splitV v1 v2 v3 a1) (splitV v1 v2 v3 a2)
