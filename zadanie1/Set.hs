module Set(Set(..), empty, null, singleton, union, fromList
              , member, toList, toAscList, elems, cartProd
              , remDup, union', sub, unpackTuples, show'
              ) where
import Prelude hiding(null)
import qualified Data.List as L

data Set a = Empty
           | Singleton a
           | Union (Set a) (Set a)

empty :: Set a
empty = Empty

null :: Set a -> Bool
null Empty = True
null _ = False

member :: Eq a => a -> Set a -> Bool
member _ Empty = False
member x (Singleton a) = x == a
member x (Union a1 a2) = if member x a1 || member x a2 then True else False

singleton :: a -> Set a
singleton = Singleton

fromList :: [a] -> Set a
fromList [] = Empty
fromList [x] = Singleton x
fromList (x:xs) = Union (Singleton x) (fromList xs)

toList :: Set a -> [a]
toList Empty = []
toList (Singleton a) = [a]
toList (Union a1 a2) = toList a1 ++ toList a2

toAscList :: Ord a => Set a -> [a]
toAscList x = (remFromSorted  . L.sort . toList) x

remFromSorted :: Ord a => [a] -> [a]
remFromSorted [] = []
remFromSorted [x] = [x]
remFromSorted (x:xs) = if x == head xs then remFromSorted xs else x : remFromSorted xs

elems :: Set a -> [a]
elems = toList

union :: Set a -> Set a -> Set a
union Empty a = a
union a Empty = a
union a1 a2 = Union a1 a2

union' :: (Ord a) => Set a -> Set a -> Set a
union' a1 a2 = fromList (L.union (toList a1) (toList a2))

insert :: a -> Set a -> Set a
insert x a = Union a (Singleton x)

sub :: (Eq a) => Set a -> Set a -> Set a
sub x y = fromList ((toList x) L.\\ (toList y))

unpackTuples :: Set (a,a) -> Set a
unpackTuples a = let list = toList a
                 in fromList $ [fst | (fst,_) <- list] ++ [snd | (_,snd) <- list]

show' :: (Ord a, Show a) => Set a -> String
show' = show . L.sort . toList

cartProd :: Set a -> Set a -> Set (a, a)
cartProd a1 a2 = fromList [(x1, x2) | x1 <- toList a1, x2 <- toList a2]

remDup :: (Ord a) => Set a -> Set a
remDup = fromList . toAscList

instance Ord a => Eq (Set a) where
  (==) a1 a2 = (L.nub . toAscList) a1 == (L.nub . toAscList) a2

instance Semigroup (Set a) where
  (<>) = union

instance Monoid (Set a) where
  mempty = empty

instance Show a => Show (Set a) where
  show = show . toList

instance Functor Set where
  fmap f = fromList . map f . toList