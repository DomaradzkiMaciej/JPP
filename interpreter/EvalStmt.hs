module EvalStmt where

import Data.Map as Map
import Data.Maybe
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Except

import AbsDuc
import PrintDuc ( printTree )
import TypesAndData
import EvalExp
import EvalDecl

evalDecrIncr :: (Int -> Int) -> String -> Ident -> Semantic (Maybe NVoidVal)

evalDecrIncr fun funName ident = do
  (vars, _) <- ask
  case Map.lookup ident vars of
    Just loc -> do
      var <- gets $ Map.lookup loc
      case var of
        Just (INTEGER val) -> do
          modify $ Map.insert loc $ INTEGER $ fun val
          return Nothing
        _ -> throwError $ "Error: Usage of " ++ funName ++ " with non int value\n" ++ show ident
    Nothing -> throwError $ "Error: Usage of undeclared variable\n" ++ show ident

evalStmt :: Stmt -> Semantic (Maybe NVoidVal)

evalStmt Empty = return Nothing

evalStmt (BStmt blc) = do
  let (Blc stmts) = blc
  evalStmts stmts

evalStmt (Ass ident exp) = do
  (vars, _) <- ask
  case Map.lookup ident vars of
    Just id -> do
      val2 <- evalExp exp
      Just val1 <- gets $ Map.lookup id
      case val1 of
        INTEGER v1 -> do
          case val2 of
            Just (INTEGER v2) -> do
              modify $ Map.insert id (INTEGER v2)
              return Nothing
            _ -> throwError $ "Error: Attempt to assign value of bad type\n" ++ show ident
        BOOL v1 -> do
          case val2 of
            Just (BOOL v2) -> do
              modify $ Map.insert id (BOOL v2)
              return Nothing
            _ -> throwError $ "Error: Attempt to assign value of bad type\n" ++ show ident
        STRING v1 -> do
          case val2 of
            Just (STRING v2) -> do
              modify $ Map.insert id (STRING v2)
              return Nothing
            _ -> throwError $ "Error: Attempt to assign value of bad type\n" ++ show ident
    Nothing -> throwError $ "Error: usage of undeclared variable\n" ++ show ident

evalStmt (Incr ident) = evalDecrIncr ((+) 1) "incrementation" ident

evalStmt (Decr ident) = evalDecrIncr (flip (-) 1) "decrementation" ident

evalStmt (Ret exp) = evalExp exp

evalStmt VRet = return Nothing

evalStmt (Cond exp blc) = do
  e <- evalExp exp
  let (Blc stmts) = blc
  case e of
    Just (BOOL True) -> evalStmts stmts
    Just (BOOL False) -> return Nothing
    _ -> throwError $ "Error: Non bool value in if condition\n" ++ printTree exp

evalStmt (CondElse exp blc1 blc2) = do
  e <- evalExp exp
  let (Blc stmts1) = blc1
  let (Blc stmts2) = blc2
  case e of
    Just (BOOL True) -> evalStmts stmts1
    Just (BOOL False) -> evalStmts stmts2
    _ -> throwError $ "Error: Non bool value in if condition\n" ++ printTree exp

evalStmt (While exp blc) = do
  e <- evalExp exp
  let (Blc stmts) = blc
  case e of
    Just (BOOL True) -> do
      evalStmts stmts
      evalStmt (While exp blc)
    Just (BOOL False) -> return Nothing
    _ -> throwError $ "Error: Non bool value in while condition\n" ++ printTree exp

evalStmt (SExp exp) = do
  evalExp exp
  return Nothing

evalStmt (Print exp) = do
  e <- evalExp exp
  case e of
    Just (INTEGER val) -> do
      liftIO $ print val
      return Nothing
    Just (STRING val) -> do
      liftIO $ print val
      return Nothing
    Just (BOOL val) -> do
      liftIO $ print val
      return Nothing

funDecl :: Type -> Ident -> [Arg] -> Block -> Semantic Environment

funDecl typ ident args blc = do
  let (Blc stmts) = blc
  let fun pars = do {
    (vars, funs) <- ask;
    (vars', funs') <- local (const (vars, funs)) (makeEnvWithArgs $ getArgs args pars);
    local (const (vars', Map.insert ident (Function fun) funs')) (runFunction stmts typ)
    }

  (vars, funs) <- ask
  return (vars, Map.insert ident (Function fun) funs)

evalStmts :: [Stmt] -> Semantic (Maybe NVoidVal)

evalStmts [] = return Nothing

evalStmts (stmt:rest) = case stmt of
  (Decl typ items) -> do
    env <- inDecl typ items
    local (const env) (evalStmts rest)
  (FnDecl typ ident args blc) -> do
    env <- funDecl typ ident args blc
    local (const env) (evalStmts rest)
  _ -> do
    val <- evalStmt stmt
    case val of
      Just v -> return $ Just v
      Nothing -> evalStmts rest

runFunction :: [Stmt] -> Type -> Semantic (Maybe NVoidVal)

runFunction stmts typ = do
  v <- evalStmts stmts
  case v of
    Nothing -> case typ of
      (Type Void) -> return v
      _ -> throwError $ "Error: lack of returning value in non void function\n"
    Just (INTEGER val) -> case typ of
      Type Int -> return v
      Type Void -> throwError $ "returning value from void function\n" ++ show val
      _ -> throwError $ "Error: bad type of returning value\n" ++ show val
    Just (BOOL val) -> case typ of
      Type Bool -> return v
      Type Void -> throwError $ "returning value from void function\n" ++ show val
      _ -> throwError $ "Error: bad type of returning value\n" ++ show val
    Just (STRING val) -> case typ of
      Type Str -> return v
      Type Void -> throwError $ "returning value from void function\n" ++ show val
      _ -> throwError $ "Error: bad type of returning value\n" ++ show val
