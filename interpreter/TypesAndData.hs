module TypesAndData where

import AbsDuc
import Data.Map as M
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Except
import Data.Maybe

type Environment = (M.Map Ident Int, M.Map Ident Function)
type Semantic = ReaderT Environment (StateT Statement (ExceptT String IO))
type Statement = M.Map Int NVoidVal

data Function = Function ([Expr] -> Semantic (Maybe NVoidVal))
data NVoidVal = INTEGER Int | BOOL Bool | STRING String