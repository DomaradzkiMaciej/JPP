void superFunction (int i1, int & i2) {
  print i1;                     // 4
  print i2;                     // 7

  i2 = i2 + 1;
  return;
}

int i3 = 7;
superFunction(4, i3);

print i3;                       // 8