module EvalDecl where

import Data.Map as Map
import Data.Maybe
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Except

import AbsDuc
import PrintDuc ( printTree )
import TypesAndData
import EvalExp

varDeclWithValue :: Ident -> NVoidVal -> Semantic Environment

varDeclWithValue ident val = do
  Just (INTEGER oldID) <- gets $ Map.lookup 0
  let newID = oldID + 1
  modify $ Map.insert 0 $ INTEGER $ newID
  modify $ Map.insert newID val
  (vars, funs) <- ask
  return (Map.insert ident newID vars, funs)

varDecl :: Type -> Ident -> Semantic Environment

varDecl (Type Int) ident = varDeclWithValue ident (INTEGER 0)

varDecl (Type Str) ident = varDeclWithValue ident (STRING "")

varDecl (Type Bool) ident = varDeclWithValue ident (BOOL True)

varDeclWithAssigment :: Type -> Ident -> Expr -> Semantic Environment

varDeclWithAssigment (Type Int) ident exp = do
  v <- evalExp exp
  case v of
    Just (INTEGER val) -> varDeclWithValue ident (INTEGER val)
    _ -> throwError $ "Error: Assigment of non int value to int variable\n" ++ show ident

varDeclWithAssigment (Type Str) ident exp = do
  v <- evalExp exp
  case v of
    Just (STRING val) -> varDeclWithValue ident (STRING val)
    _ -> throwError $ "Error: Assigment of non string value to string variable\n" ++ show ident

varDeclWithAssigment (Type Bool) ident exp = do
  v <- evalExp exp
  case v of
    Just (BOOL val) -> varDeclWithValue ident (BOOL val)
    _ -> throwError $ "Error: Assigment of non int bool to bool variable\n" ++ show ident

refDecl :: Type -> Ident -> Expr -> Semantic Environment

refDecl (Type Int) ident1 (EVar ident2) = do
  (vars, funs) <- ask
  case Map.lookup ident2 vars of
    Just id -> do
      Just val <- gets $ Map.lookup id
      case val of
        (INTEGER _) -> return (Map.insert ident1 id vars, funs)
        _ -> throwError $ "Error: Assigment of non int value to int variable\n" ++ show ident2
    _ -> throwError $ "Error: Usage of undeclared variable\n" ++ show ident2

refDecl (Type Str) ident1 (EVar ident2) = do
  (vars, funs) <- ask
  case Map.lookup ident2 vars of
    Just id -> do
      Just val <- gets $ Map.lookup id
      case val of
        (STRING _) -> return (Map.insert ident1 id vars, funs)
        _ -> throwError $ "Error: Assigment of non string value to string variable\n" ++ show ident2
    _ -> throwError $ "Error: Usage of undeclared variable\n" ++ show ident2

refDecl (Type Bool) ident1 (EVar ident2) = do
  (vars, funs) <- ask
  case Map.lookup ident2 vars of
    Just id -> do
      Just val <- gets $ Map.lookup id
      case val of
        (BOOL _) -> return (Map.insert ident1 id vars, funs)
        _ -> throwError $ "Error: Assigment of non bool value to bool variable\n" ++ show ident2
    _ -> throwError $ "Error: Usage of undeclared variable\n" ++ show ident2

refDecl _ _ exp = throwError $ "Error: References must be used with an identifier \n" ++ printTree exp

inDecl :: Type -> [Item] -> Semantic Environment

inDecl typ [] = ask

inDecl typ (item:rest) = case item of
  (Init ident exp) -> do
    env <- varDeclWithAssigment typ ident exp
    local (const env) (inDecl typ rest)
  (NoInit ident) -> do
    env <- varDecl typ ident
    local (const env) (inDecl typ rest)

makeEnvWithArgs :: [(Type, Ident, Expr, Bool, Bool)] -> Semantic Environment

makeEnvWithArgs [] = ask

makeEnvWithArgs ((typ, ident, exp, isRef, isTooMany):rest) = case ident of
  (Ident "") -> case isTooMany of
    True -> throwError $ "Error: Too many arguments\n" ++ show ident
    False -> throwError $ "Error: Not enough arguments\n" ++ show ident
  _ -> case isRef of
    False -> do
      env <- varDeclWithAssigment typ ident exp
      local (const env) (makeEnvWithArgs rest)
    True -> do
      env <- refDecl typ ident exp
      local (const env) (makeEnvWithArgs rest)

getArgs :: [Arg] -> [Expr] -> [(Type, Ident, Expr, Bool, Bool)]

getArgs [] [] = []

getArgs [] _ = [(Type Bool, Ident "", EVar (Ident ""), False, True)]

getArgs _ [] = [(Type Bool, Ident "", EVar (Ident ""), False, False)]

getArgs (arg:args) (exp:exps) = case arg of
  (ArgVal typ ident) -> (typ, ident, exp, False, False) : (getArgs args exps)
  (ArgRef typ ident) -> (typ, ident, exp, True, False) : (getArgs args exps)