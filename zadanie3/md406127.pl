% Domaradzki Maciej
% State of program is represented by term containing 3 arrays. First have values of Variables, secong values of
% array and the last one have actual position of each process (current instruction).

:- ensure_loaded(library(lists)).
:- op(700, xfx, <>).


verify(N, Program) :-
  ( integer(N), N>0 ) ->
    ( Goal = (open(Program, read, Stream), verifyProgram(N, Stream)),
      ExceptionTerm = error(existence_error(_, _), context(system:open/3, _)),
      RecoveryGoal = (format('Error: brak pliku o nazwie - ~s\n', [Program])),
      catch(Goal, ExceptionTerm, RecoveryGoal)
    );
    ( write('Error: parametr 0 powinien byc liczba > 0\n') ).

verifyProgram(N, Stream) :-
  readFile(Stream, Program),
  initState(Program, N, InitState),
  check(InitState, N, Program, [], _, Res),
  (Res = programIsSafe ->
    write('Program jest poprawny (bezpieczny).\n');
    writeIfUnsafe(Res)
  ).

readFile(Stream, Program) :-
  read(Stream, variables(Vars)),
  read(Stream, arrays(Arrays)),
  read(Stream, program(Prog)),
  close(Stream),
  Program = program(Vars, Arrays, Prog).

initState(program(Vars, Arrs, _), N, state(InVars, InArrs, Pos)) :-
  initVars(Vars, InVars),
  initArrs(Arrs, N, InArrs),
  zeros(N, Pos).

initVars([], []).
initVars([V | Rest], [map(V, 0) | OutRest]) :-
  initVars(Rest, OutRest).

initArrs([], _, []).
initArrs([V | Rest], N, [map(V, Arr) | OutRest]) :-
  zeros(N, Arr),
  initArrs(Rest, N, OutRest).

zeros(0, []).
zeros(N, [0 | Rest]) :-
  N > 0,
  M is N-1,
  zeros(M, Rest).

writeIfUnsafe(err(Interlacing, [Proc1, Proc2 | _])) :-
  write('Program jest niepoprawny.\n'),
  write('Niepoprawny przeplot:\n'),
  writeInterlacing(Interlacing),
  format('Procesy w sekcji: ~d, ~d.\n', [Proc1, Proc2]).

writeInterlacing([]).
writeInterlacing([map(Proc, Pos) | Rest]) :-
  format('   Proces ~d: ~d\n', [Proc, Pos]),
  writeInterlacing(Rest).

check(CurrentState, N, Program, VisitedStates, OutVisitedStates, Res) :-
  checkSafety(Program, CurrentState, IsSafe, ProcessesInSection),
  ( IsSafe ->
    ( append(VisitedStates, [CurrentState], NewVisitedStates),
      getNextStates(CurrentState, N, Program, NewStates),
      checkNextStates(NewStates, N, Program, NewVisitedStates, OutVisitedStates, Res)
    );
    Res = err([], ProcessesInSection)
  ).

checkSafety(program(_, _, Prog), state(_, _, Pos), IsSafe, ProcessesInSection) :-
  countProcInSection(Prog, Pos, 0, Counter, ProcessesInSection),
  ( Counter < 2 ->
    IsSafe = true;
    IsSafe = false
  ).

countProcInSection(_, [], _, 0, []).
countProcInSection(Prog, [Pos | Rest], Idx, Counter, ProcessesInSection) :-
  NextIdx is Idx+1,
  countProcInSection(Prog, Rest, NextIdx, NewCounter, NewProcessesInSection),
  nth0(Pos, Prog, Statement),
  ( Statement == sekcja  ->
    ( Counter is NewCounter+1, ProcessesInSection = [Idx | NewProcessesInSection] );
    ( Counter = NewCounter, ProcessesInSection = NewProcessesInSection )
  ).

getNextStates(_, 0, _, []).
getNextStates(State, Proc, Program, OutNewStates) :-
  Proc > 0,
  NewProc is Proc-1,
  state(_, _, Positions) = State,
  nth0(NewProc, Positions, Pos),
  OldPos is Pos+1,
  step(Program, State, NewProc, NewState),
  getNextStates(State, NewProc, Program, NewStates),
  OutNewStates = [nexts(NewProc, OldPos, NewState) | NewStates].

checkNextStates([], _, _, VisitedStates, VisitedStates, programIsSafe).
checkNextStates([nexts(_, _, State) | Rest], N, Program, VisitedStates, OutVisitedStates, Res) :-
  member(State, VisitedStates),
  checkNextStates(Rest, N, Program, VisitedStates, OutVisitedStates, Res).

checkNextStates([nexts(Proc, Pos, State) | Rest], N, Program, VisitedStates, OutVisitedStates, Res) :-
  \+ member(State, VisitedStates),
  check(State, N, Program, VisitedStates, NewVisitedStates, NewRes),
  ( NewRes == programIsSafe ->
    checkNextStates(Rest, N, Program, NewVisitedStates, OutVisitedStates, Res);
    ( err(Interlacing, Processes) = NewRes, Res = err([map(Proc, Pos) | Interlacing], Processes))
  ).

step(program(_, _, Prog), state(Vars, Arrs, Pos), PID, OutState) :-
  nth0(PID, Pos, Idx),
  nth0(Idx, Prog, Statement),
  evalStmt(Statement, state(Vars, Arrs, Pos), PID, OutState).

evalStmt(sekcja, state(Vars, Arrs, Pos), PID, OutState) :-
  nextPos(PID, Pos, NewPos),
  OutState = state(Vars, Arrs, NewPos).

evalStmt(assign(Id, Expr), state(Vars, Arrs, Pos), PID, OutState) :-
  atom(Id),
  evalExpr(Expr, PID, state(Vars, Arrs, Pos), Val),
  replaceEl(map(Id, _), map(Id, Val), Vars, NewVars),
  nextPos(PID, Pos, NewPos),
  OutState = state(NewVars, Arrs, NewPos).

evalStmt(assign(array(Id, IdExpr), Expr), state(Vars, Arrs, Pos), PID, OutState) :-
  member(map(Id, Arr), Arrs),
  evalExpr(Expr, PID, state(Vars, Arrs, Pos), Val),
  evalExpr(IdExpr, PID, state(Vars, Arrs, Pos), Idx),
  replace(Idx, Val, Arr, NewArr),
  replaceEl(map(Id, Arr), map(Id, NewArr), Arrs, NewArrs),
  nextPos(PID, Pos, NewPos),
  OutState = state(Vars, NewArrs, NewPos).

evalStmt(goto(Idx), state(Vars, Arrs, Pos), PID, OutState) :-
  NewIdx is Idx-1,
  newPos(PID, NewIdx, Pos, NewPos),
  OutState = state(Vars, Arrs, NewPos).

evalStmt(condGoto(BoolExpr, Idx), state(Vars, Arrs, Pos), PID, OutState) :-
  NewIdx is Idx-1,
  evalBoolExpr(BoolExpr, PID, state(Vars, Arrs, Pos), Res),
  ( Res ->
    newPos(PID, NewIdx, Pos, NewPos);
    nextPos(PID, Pos, NewPos)
  ),
  OutState = state(Vars, Arrs, NewPos).

newPos(Pid, Idx, Pos, NewPos) :-
  replace(Pid, Idx, Pos, NewPos).

nextPos(PID, Pos, NewPos) :-
  nth0(PID, Pos, Idx),
  NewIdx is Idx+1,
  newPos(PID, NewIdx, Pos, NewPos).

replace(Idx, Value, Array, NewArray) :-
  nth0(Idx, Array, _, R),
  nth0(Idx, NewArray, Value, R).

replaceEl(_, _, [], []).
replaceEl(OldEl, NewEl, [OldEl | Rest], [NewEl | Rest]).
replaceEl(OldEl, NewEl, [El | Rest], [El | OutRest]) :-
  OldEl \= NewEl,
  replaceEl(OldEl, NewEl, Rest, OutRest).

evalBoolExpr(Expr1 = Expr2, PID, State, Val) :-
  evalExpr(Expr1, PID, State, Val1),
  evalExpr(Expr2, PID, State, Val2),
  ( Val1 =:= Val2 ->
    Val = true;
    Val = false
  ).

evalBoolExpr(Expr1 < Expr2, PID, State, Val) :-
  evalExpr(Expr1, PID, State, Val1),
  evalExpr(Expr2, PID, State, Val2),
  ( Val1 < Val2 ->
    Val = true;
    Val = false
  ).

evalBoolExpr(Expr1 <> Expr2, PID, State, Val) :-
  evalExpr(Expr1, PID, State, Val1),
  evalExpr(Expr2, PID, State, Val2),
  ( Val1 =\= Val2 ->
    Val = true;
    Val = false
  ).

evalExpr(pid, PID, _, PID).
evalExpr(Expr, _, _, Expr) :- integer(Expr).
evalExpr(Id, _, state(Vars, _, _), Val) :-
  atom(Id),
  member(map(Id, Val), Vars).

evalExpr(array(Id, Expr), PID, state(Vars, Arrs, Pos), Val) :-
  evalExpr(Expr, PID, state(Vars, Arrs, Pos), Idx),
  member(map(Id, Values), Arrs),
  nth0(Idx, Values, Val).

evalExpr(Expr1 + Expr2, PID, State, Val) :-
  evalExpr(Expr1, PID, State, Val1),
  evalExpr(Expr2, PID, State, Val2),
  Val is Val1 + Val2.

evalExpr(Expr1 - Expr2, PID, State, Val) :-
  evalExpr(Expr1, PID, State, Val1),
  evalExpr(Expr2, PID, State, Val2),
  Val is Val1 - Val2.

evalExpr(Expr1 * Expr2, PID, State, Val) :-
  evalExpr(Expr1, PID, State, Val1),
  evalExpr(Expr2, PID, State, Val2),
  Val is Val1 * Val2.

evalExpr(Expr1 / Expr2, PID, State, Val) :-
  evalExpr(Expr1, PID, State, Val1),
  evalExpr(Expr2, PID, State, Val2),
  Val is Val1 / Val2.