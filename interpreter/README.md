# Interpreter of language with C-like syntax

My language is based on Latte language (https://www.mimuw.edu.pl/~ben/Zajecia/Mrj2019/Latte/). The difference is that in my language is print (usage: print exp), curly brackets are obligatory
in "if else" and in "while" and program consists of statements, so you can for example use print outside the function. You can
declare multiple variables or functions with the same name in the same scope. In that situation the next declaration shadows
the previous one (but you can used the previous definition between the first declaration and the second).

Exaples of programs are in catalog examples (in subcatalog good are correct programs and in subcatalog bad are uncorrect ptograms).

To compile the program, run make in the main folder.
To run the program, type ./interpreter <path to program>

realized points
  Na 15 punktów
  01 (trzy typy) +
  02 (literały, arytmetyka, porównania) +
  03 (zmienne, przypisanie) +
  04 (print) +
  05 (while, if) +
  06 (funkcje lub procedury, rekurencja) +
  07 (przez zmienną / przez wartość / in/out) +
  08 (zmienne read-only i pętla for)
  Na 20 punktów
  09 (przesłanianie i statyczne wiązanie) +
  10 (obsługa błędów wykonania) +
  11 (funkcje zwracające wartość) +
  Na 30 punktów
  12 (4) (statyczne typowanie)
  13 (2) (funkcje zagnieżdżone ze statycznym wiązaniem) +
  14 (1) (rekordy/tablice/listy)
  15 (2) (krotki z przypisaniem)
  16 (1) (break, continue)
  17 (4) (funkcje wyższego rzędu, anonimowe, domknięcia)
  18 (3) (generatory)

Razem: 22
